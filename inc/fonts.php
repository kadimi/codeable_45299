<?php

if ( ! function_exists( 'et_get_one_font_languages' ) ) :
function et_get_one_font_languages() {
	$one_font_languages = array(
		'he_IL' => array(
			'language_name'   => 'Hebrew',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/alefhebrew.css',
			'font_family'     => "'Alef Hebrew', serif",
		),
		'ja' => array(
			'language_name'   => 'Japanese',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/notosansjapanese.css',
			'font_family'     => "'Noto Sans Japanese', serif",
		),
		'ko_KR' => array(
			'language_name'   => 'Korean',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/hanna.css',
			'font_family'     => "'Hanna', serif",
		),
		'th' => array(
			'language_name'   => 'Thai',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/notosansthai.css',
			'font_family'     => "'Noto Sans Thai', serif",
		),
		'ms_MY' => array(
			'language_name'   => 'Malay',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/notosansmalayalam.css',
			'font_family'     => "'Noto Sans Malayalam', serif",
		),
		'zh_CN' => array(
			'language_name'   => 'Chinese',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/cwtexfangsong.css',
			'font_family'     => "'cwTeXFangSong', serif",
		),
	);

	return $one_font_languages;
}
endif;

if ( ! function_exists( 'et_builder_get_fonts' ) ) :
function et_builder_get_fonts( $settings = array() ) {

	$defaults = array(
		'prepend_standard_fonts' => true,
	);

	$settings = wp_parse_args( $settings, $defaults );

	$fonts = $settings['prepend_standard_fonts']
		? array_merge( et_builder_get_websafe_fonts(), et_builder_get_google_fonts() )
		: array_merge( et_builder_get_google_fonts(), et_builder_get_websafe_fonts() );

	$fonts[ 'Droid Arabic Naskh' ] = [
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'sans-serif',
	];
	$fonts[ 'Droid Arabic Kufi' ] = [
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'sans-serif',
	];

	ksort( $fonts );

	return $fonts;
}
endif;
