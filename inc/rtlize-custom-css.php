<?php

// add_filter( 'et_builder_post_types', '__return_empty_array' );


add_action( 'init', function() {

	/**
	 * Run once.
	 */
	static $once;
	if ( $once ) {
		return;
	}
	$once = true;

	/**
	 * Run only for RTL languages.
	 */
	if ( ! is_rtl() ) {
		return;
	}

	/**
	 * Remove previous
	 */
	remove_action( 'wp_head', 'et_add_custom_css', 100 );
	add_action( 'wp_head', function() {
		global $shortname;
		
		$custom_css = et_get_option( "{$shortname}_custom_css" );

		if ( false === $custom_css || '' == $custom_css ) return;

		$custom_css = CSSJanus::transform( $custom_css );

		/**
		 * The theme doesn't strip slashes from custom css, when saving to the database,
		 * so it does that before outputting the code on front-end
		 */
		echo '<style type="text/css" id="et-custom-css">' . "\n" . stripslashes( $custom_css ) . "\n" . '</style>';
	}, 100 );
} );
