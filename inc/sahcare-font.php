<?php

add_action( 'the_content', 'sahcare_font', 999999 );
add_action( 'nav_menu_item_title', 'sahcare_font', 999999 );

function sahcare_font( $content ) {
	/**
	 * Check if this is an RTL language
	 */
	if ( ! is_rtl() ) {
		return $content;
	}

	$content = str_replace( 'SAH Care', '<span style="font-family:lato">SAH Care</span>', $content );

	return $content;
} 
